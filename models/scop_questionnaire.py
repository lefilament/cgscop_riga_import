# © 2019 Le Filament (<http://www.le-filament.com>)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from dateutil.relativedelta import relativedelta

from odoo import models, fields, api


class ScopQuestionnaireImport(models.Model):
    _inherit = "scop.questionnaire"

    year_riga = fields.Char("Année Riga")

    type_id = fields.Many2one(compute="_compute_fields", store=True)
    year = fields.Char(compute="_compute_fields", store=True)
    effective_date = fields.Date(compute="_compute_fields", store=True)

    # ------------------------------------------------------
    # Fonctions compute
    # ------------------------------------------------------
    @api.depends('year_riga')
    @api.multi
    def _compute_fields(self):
        for questionnaire in self:
            if questionnaire.year_riga != '14862' and questionnaire.year_riga != '14443':
                year = questionnaire.env['riga.lookup.table'].search([
                    ['id_riga', '=', questionnaire.year_riga]])
                questionnaire.year = year.name
                questionnaire.type_id = questionnaire.env.ref(
                    'cgscop_partner.questionnaire_type_2').id
                questionnaire.effective_date = fields.Date.to_date(year.name + '-12-31') - relativedelta(years=1)
            else:
                if questionnaire.partner_id.date_1st_sign:
                    questionnaire.year = questionnaire.partner_id.date_1st_sign.year
                    questionnaire.effective_date = questionnaire.partner_id.date_1st_sign
                questionnaire.type_id = self.env.ref(
                    'cgscop_partner.questionnaire_type_1').id
