# © 2019 Le Filament (<http://www.le-filament.com>)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from odoo import models, fields, api


class ScopQuestionnaireImport(models.Model):
    _inherit = "scop.liasse.fiscale"

    year_riga = fields.Char("Année Riga")
    type_id = fields.Many2one(compute="_compute_fields", store=True)
    year = fields.Char(compute="_compute_fields", store=True)

    # ------------------------------------------------------
    # Fonctions compute
    # ------------------------------------------------------
    @api.depends('year_riga')
    @api.multi
    def _compute_fields(self):
        for liasse in self:
            if liasse.year_riga != '14443':
                year = liasse.env['riga.lookup.table'].search([
                    ['id_riga', '=', liasse.year_riga]])
                liasse.year = year.name
                liasse.type_id = liasse.env.ref(
                    'cgscop_partner.liasse_type_2').id
            else:
                if liasse.partner_id.date_1st_sign:
                    liasse.year = liasse.partner_id.date_1st_sign.year
                liasse.type_id = liasse.env.ref(
                    'cgscop_partner.liasse_type_1').id

