# © 2019 Le Filament (<http://www.le-filament.com>)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from odoo import models, fields


class CgscopRigaFilesMatchingHeader(models.Model):
    _name = 'riga.files.matching.header'
    _description = 'Correspondance champ et header csv'

    parent_id = fields.Many2one(
        comodel_name='riga.files.matching',
        string='Modèle')
    name = fields.Char('Champ Odoo', required=True)
    riga_name = fields.Char('Header RIGA', required=True)
    relation = fields.Char('Champ Relation')


class CgscopRigaFilesMatching(models.Model):
    """ Ce modèle d'ajouter des fichier à importer depuis RIGA
    et de définir les correspondances associées
    """
    _name = 'riga.files.matching'
    _description = 'Synchronisation RIGA vers Odoo'
    _rec_name = 'model_id'

    model_id = fields.Many2one(
        comodel_name='ir.model',
        string='Modèle')
    riga_filename = fields.Char('Nom Fichier RIGA')
    riga_extension = fields.Char('Extension Fichier')
    is_active = fields.Boolean("Actif", default=True)
    header_key = fields.Char("Nom clé parent", default='PMID')
    primary_key = fields.Char("Clé primaire", default='EVTID')
    matching_table_ids = fields.One2many(
        comodel_name='riga.files.matching.header',
        inverse_name='parent_id',
        string='Table de correspondance Odoo - Riga')
    suffix = fields.Char(
        string="Suffixe",
        trim=False)
    day = fields.Integer("Décalage jour", default=False)
    params_active = fields.Boolean('Extra params actifs')
    ftp_channel_id = fields.Many2one(
        comodel_name='riga.ftp.channel',
        string="Canal FTP")

    def sync_data(self):
        odoo_import = self.env['riga.odoo.import'].sudo()
        odoo_import.sync_file(self)
