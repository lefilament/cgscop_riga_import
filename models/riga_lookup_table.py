# © 2019 Le Filament (<http://www.le-filament.com>)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from odoo import models, fields


class CgscopRigaLookupTable(models.Model):
    _name = 'riga.lookup.table'
    _description = 'Correspondance valeur et id RIGA'
    _sql_constraints = [
        ('id_riga_uniq',
            'UNIQUE (id_riga)',
            'You can not have two fields with the same RIGA id !')
    ]

    name = fields.Char('Valeur RIGA')
    id_riga = fields.Char('ID RIGA')
