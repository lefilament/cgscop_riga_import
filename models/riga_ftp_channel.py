# © 2019 Le Filament (<http://www.le-filament.com>)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from odoo import models, fields


class CgscopRigaFtpChannel(models.Model):
    _name = 'riga.ftp.channel'
    _description = 'Infos FTP de connexion pour fichiers RIGA'

    name = fields.Char(string='Nom')
    login = fields.Char('Login')
    password = fields.Char('Password')
    url = fields.Char('URL')
    path = fields.Char('Path')
    active = fields.Boolean('Actif', default=True)
