# © 2019 Le Filament (<http://www.le-filament.com>)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

import random
import logging

from odoo.tests import tagged, common

_logger = logging.getLogger(__name__)


@tagged('post_install', '-at_install')
class TestApiRiga(common.TransactionCase):

    def test_read(self):
        print("---------- tests riga ----------------")
        # riga = self.env['riga.api']
        # print("______ Lecture ______")
        # print(riga.read_organism(6481))

        print("\n______ Queue ______\n")
        queue = self.env['riga.job.queue']
        queue.riga_sync()
