.. image:: https://img.shields.io/badge/licence-AGPL--3-blue.svg
   :target: http://www.gnu.org/licenses/agpl
   :alt: License: AGPL-3


======================
CG SCOP - Imports RIGA
======================

Description
===========

Ce module permet d'importer quotidiennement les modifications de RIGA qui remontent en lecture seule dans Odoo.

Chaque nuit, Odoo va lire les fichiers RIGA stockés sur le FTP Eudonet, et mettre à jour les différentes tables sélectionnées. Par défaut, il va mettre à jour les tables suivantes : 

* **scop.contribution**
* **scop.questionnaire**
* **scop.liasse.fiscale**

La liste des modèles et fichiers à synchroniser est passée en paramètre (tableau de tuples) dans le CRON prévu à cet effet : *CG Scop - Synchro RIGA vers Odoo*.

Usage
=====

Pour configurer ce module, les données suivantes sont à renseigner 

1. dans le menu **Configuration > APIs CG Scop > RIGA > FTP Imports RIGA** : 

* *nom* : nom de la connexion
* *login* : login de connexion au FTP
* *password* : password de connexion au FTP
* *active* : si il s'agit de la connexion active

Attention, il faut qu'il n'existe qu'une seule connexion active.

2. dans le menu **Contacts > Configuration > Impot RIGA**

* le menu *Configuration imports RIGA* permet de renseigner les différentes tables à mettre à jour avec : le nom de la table, le nom du fichier, la clé primaire, la clé étrangère (parent) et la table de correspondance
* le menu *RIGA vers Odoo* permet de visualiser les détails des imports de chaque fichier


Credits
=======

Funders
------------

The development of this module has been financially supported by:

    Confédération Générale des SCOP (https://www.les-scop.coop)


Contributors
------------

* Juliana Poudou <juliana@le-filament.com>
* Rémi Cazenave <remi@le-filament.com>
* Benjamin Rivier <benjamin@le-filament.com>


Maintainer
----------

.. image:: https://le-filament.com/images/logo-lefilament.png
   :alt: Le Filament
   :target: https://le-filament.com

This module is maintained by Le Filament
