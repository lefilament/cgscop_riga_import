{
    "name": "CG SCOP - Imports RIGA",
    "summary": "Imports RIGA quotidiens",
    "version": "12.0.1.0.1",
    "development_status": "Beta",
    "author": "Le Filament",
    "license": "AGPL-3",
    "application": False,
    "depends": [
        'base',
        'web',
        'cgscop_partner',
    ],
    "data": [
        "security/ir.model.access.csv",
        "views/riga_files_matching.xml",
        "views/riga_ftp_channel.xml",
        "views/riga_lookup_table.xml",
        "views/riga_odoo_import.xml",
        "datas/ir_config_parameter.xml",
        "datas/cron_riga_odoo.xml",
        "datas/riga_files_matching_data.xml",
        "datas/riga_files_matching_header_data.xml",
        "datas/riga_lookup_table_data.xml",
    ],
    'installable': True,
    'auto_install': False,
}
